#+BEGIN_html
<a rel="license" href="http://creativecommons.org/licenses/by/3.0/za/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/za/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/za/">Creative Commons Attribution 3.0 South Africa License</a>.
#+END_html

* /Chrysoporthe cubensis/ CMW10028

This genome was sequenced using a combination of PacBio and MiSeq data.

** Publications

*** Genome announcement
   
   Wingfield B.D., I. Barnes, Z.W. de Beer, L. de Vos, T.A. Duong,
   A.M. Kanzi, K. Naidoo, H.D.T. Nguyen, Q.C. Santana, M. Sayari,
   K.A. Seifert, E.T. Steenkamp, C. Trollip, N.A. van der Merwe,
   M.A. van der Nest, P.M. Wilken, and M.J. Wingfield. 2015. /IMA
   Fungus/ 6(2): 493-505. [[http://doi.org/10.5598/imafungus.2015.06.02.13][DOI]]

*** Other papers using this data

   Kanzi A.M., E.T. Steenkamp, N.A. van der Merwe, and
   B.D. Wingfield. 2019. The mating system of the /Eucalyptus/ canker
   pathogen /Chrysoporthe austroafricana/ and closely related
   species. 2019. /Fungal Genetics and Biology/ 123: 41-52. [[https://doi.org/10.1016/j.fgb.2018.12.001][DOI]]

   Kanzi A.M., B.D. Wingfield, E.T. Steenkamp, S. Naidoo, and N.A. van
   der Merwe. 2016. Intron derived size polymorphism in the
   mitochondrial genomes of closely related /Chrysoporthe/
   species. /PLoS ONE/ 11(6): e0156104. [[https://doi.org/10.1371/journal.pone.0156104][DOI]]

** Organism/Genome information

| Isolate       | CMW10028 =CBS118654 =PREM58788                                 |
| Species       | /Chrysoporthe cubensis/ (Bruner) Gryzenhout & M.J. Wingf. 2004 |
| NCBI Assembly | [[https://www.ncbi.nlm.nih.gov/assembly/GCA_004802525.1/][GCA_001282315.1]]                                                |


** Genome statistics

| Statistic                     | Value |
|-------------------------------+-------|
| Genome size                   |       |
| Num contigs                   |       |
| Num. genes                    |       |
| Complete BUSCOs               |       |
| Complete & Single-copy BUSCOs |       |
| Complete & Duplicated BUSCOs  |       |
| Fragmented BUSCOs             |       |
| Missing BUSCOs                |       |

